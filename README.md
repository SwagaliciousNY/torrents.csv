# Torrents.csv

`Torrents.csv` is a collaborative, vetted database of torrents, consisting of a single, searchable `torrents.csv` file. Its initially populated with a January 2017 backup of the pirate bay, and new torrents are periodically added from various torrents sites via a rust script. 

`Torrents.csv` will only store torrents with at least one seeder to keep the file small, and will be periodically purged of non-seeded torrents, and sorted by seeders descending.

## Searching
To find torrents, run `./search.sh "frasier s01"`
```
Frasier S01-S11 (1993-)
	seeders: 33
	size: 13GiB
	link: magnet:?xt=urn:btih:3cc5142d0d139bcc9ea9925239a142770b98cf74
```

## Uploading

An *upload*, consists of making a pull request after running the `add_torrents.sh` script, which adds torrents from a directory you choose to the `.csv` file, after checking that they aren't already there, and that they have seeders.

[Click here](https://gitlab.com/dessalines/torrents.csv/forks/new) to fork this repo.
```sh
git clone https://gitlab.com/[MY_USER]/torrents.csv
cd torrents.csv
./add_torrents.sh MY_TORRENTS_DIR # `MY_TORRENTS_DIR` is `~/.local/share/data/qBittorrent/BT_backup/` for qBittorrent on linux, but you can search for where your torrents are stored for your client.
git commit -am "Adding my torrents"
git push
```

Then [click here](https://gitlab.com/dessalines/torrents.csv/merge_requests/new) to do a pull/merge request to my branch.

## How the file looks
```sh
infohash;name;size_bytes;created_unix;seeders;leechers;completed;scraped_date
# torrents here...
```

## Requirements

### Searching
- [ripgrep](https://github.com/BurntSushi/ripgrep)

### Uploading
- [Torrent tracker scraper](https://github.com/ZigmundVonZaun/torrent-tracker-scraper)
- [Transmission-cli](https://transmissionbt.com/)
- [Human Friendly](https://humanfriendly.readthedocs.io/en/latest/readme.html#command-line)


## Potential sources for new torrents
- https://www.skytorrents.lol/top100
- https://1337x.to/top-100
- https://1337x.to/trending
